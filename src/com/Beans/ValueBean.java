package com.Beans;

public class ValueBean 
{
	int trustScore;
	String url;
	String feed;
	String title;
	String anchorLink;
	public String getAnchorLink() {
		return anchorLink;
	}
	public void setAnchorLink(String anchorLink) {
		this.anchorLink = anchorLink;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getTrustScore() {
		return trustScore;
	}
	public void setTrustScore(int trustScore) {
		this.trustScore = trustScore;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFeed() {
		return feed;
	}
	public void setFeed(String feed) {
		this.feed = feed;
	}
	

}
