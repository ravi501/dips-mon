package com.Logic;

import java.io.IOException;
import java.net.URL;  
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.xml.parsers.DocumentBuilder;  
import javax.xml.parsers.DocumentBuilderFactory;  

import org.w3c.dom.Document;  
import org.w3c.dom.Element;  
import org.w3c.dom.NodeList;  

import com.db.DatabaseConnection;
  
public class RSSReader {  
  
   private static RSSReader instance = null; 
   
  
   private URL rssURL;  
     
   RSSReader() {}  
  
   public static RSSReader getInstance() {  
      if (instance == null)  
         instance = new RSSReader();  
      return instance;  
   }  
  
   public void setURL(URL url) {  
      rssURL = url;  
   }  
  
   public void writeFeed(String Link) {  
      try {  
         DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();  
         Document doc = builder.parse(rssURL.openStream());  
  
         NodeList items = doc.getElementsByTagName("item"); 
         Connection con = DatabaseConnection.getConnection();
         PreparedStatement statement = con.prepareStatement("insert into javatable(source,title,description,link) values(?,?,?,?)");
         for (int i = 0; i < items.getLength(); i++) {  
            Element item = (Element)items.item(i);  
            
            statement.setString(1, Link);
			statement.setString(2, getValue(item, "title"));
			statement.setString(3, getValue(item, "description"));
			statement.setString(4, getValue(item, "link"));
			statement.executeUpdate();
			
            System.out.println(getValue(item, "title"));  
            System.out.println(getValue(item, "description"));  
            System.out.println(getValue(item, "link"));  
            System.out.println();  
         }  
      } catch (Exception e) {  
         e.printStackTrace();  
      }  
   }  
  
   public String getValue(Element parent, String nodeName) {  
      return parent.getElementsByTagName(nodeName).item(0).getFirstChild().getNodeValue();  
   }  
  
   public static void setRSSfeeds(String Link) {  
      try {  
         RSSReader reader = RSSReader.getInstance();  
         reader.setURL(new URL(Link));
         String link=Link;
 		String noHeader=link.substring(7);
 		int length=noHeader.indexOf('/');
 		String finalLink=noHeader.substring(0, length);
 		System.out.println(finalLink);
         reader.writeFeed(finalLink);  
      } catch (Exception e) {  
         e.printStackTrace();  
      }  
   }  
   public void setFeeds() throws IOException {
	   //setRSSfeeds("http://static.cricinfo.com/rss/livescores.xml");
	   setRSSfeeds("http://www.health.com/health/healthy-happy/feed");
	   setRSSfeeds("http://www.rediff.com/rss/newsrss.xml");
	   setRSSfeeds("http://finance.yahoo.com/rss/topfinstories");
	   // setRSSfeeds("http://www.tullyrankin.com/feed/rss");
	   //setRSSfeeds("http://news.google.com/?output=rss");
	   //setRSSfeeds("http://rss.ireport.com/feeds/oncnn.rss");
	  // setRSSfeeds("http://search.espn.go.com/rss/poker/");
	   setRSSfeeds("http://www.npr.org/rss/rss.php?id=1014");
	   setRSSfeeds("http://www.costar.com/News/RSS/RSS.aspx?m=COL");
	   setRSSfeeds("http://www.apa.org/news/apa-news/apa-news-rss.xml");
	   //setRSSfeeds("http://feeds.reuters.com/reuters/businessNews");
}
} 