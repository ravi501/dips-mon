package com.Logic;

import java.util.Comparator;

import com.Beans.ValueBean;

public class Sorting implements Comparator<ValueBean>
{
	@Override
	public int compare(ValueBean v1, ValueBean v2) {
		if(v1.getTrustScore() < v2.getTrustScore())
			return 1;
		else if(v1.getTrustScore() == v2.getTrustScore())
			return 0;
		else
			return -1;
	}
}