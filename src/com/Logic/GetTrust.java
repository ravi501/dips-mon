package com.Logic;

import java.io.IOException;
import java.net.UnknownHostException;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import com.Beans.TrustBean;

public class GetTrust 
{
	TrustBean trustBean = null;
	public TrustBean calculate(String Host) 
	{
		String host = Host; 
		int port = 443;
		int level;
		String Alg;
		String[] partscert;
		String CA;
		String[] parts;
		String keylength ;
		String keysize;
		String prot;
		String suite;
		
		SocketFactory factory = SSLSocketFactory.getDefault();
		SSLSocket socket = null;
		javax.security.cert.X509Certificate[] serverCerts = null;
		try {
			socket = (SSLSocket) factory.createSocket(host, port);
		
		
		socket.startHandshake();
		System.out.println("Handshaking Complete");
		
		 serverCerts = socket.getSession().getPeerCertificateChain();
		} catch (UnknownHostException e) 
		{
			e.printStackTrace();
			return trustBean;
		} catch (IOException e) 
		{
			e.printStackTrace();
			return trustBean;
		}
		trustBean = new TrustBean();
		prot = socket.getSession().getProtocol();
		suite = socket.getSession().getCipherSuite();
		level = serverCerts.length;
		
		System.out.println("Retreived Server's Certificate Chain \n");
		System.out.println("certs found "+level);
		System.out.println("protocol used is "+prot);
		System.out.println("ciphet suite is " +suite);
		
		javax.security.cert.X509Certificate myCert = serverCerts[serverCerts.length-1];
		Alg = myCert.getPublicKey().getAlgorithm();
		System.out.println("Algorithm used  " + Alg);
		
		partscert = myCert.getSubjectDN().toString().split(",");
		CA = partscert[0];
		System.out.println("Certificate Authority " + CA);
		
		parts = myCert.getPublicKey().toString().split(",");
		keylength = parts[1];
		keysize = keylength.substring(1,5);
		System.out.println("key length "+keysize);
		
		trustBean.setSourceName(Host);
		trustBean.setLevel(level);
		trustBean.setCA(CA);
		trustBean.setAlgorithm(Alg);
		trustBean.setKeySize(keysize);
		trustBean.setProtocol(prot);
		trustBean.setCipher(suite);
		
		System.out.println();
		return trustBean;
	}

}
