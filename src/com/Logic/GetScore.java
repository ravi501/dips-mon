package com.Logic;

import java.util.Map;
import java.util.TreeMap;

import com.Beans.TrustBean;

public class GetScore 
{
	public static Map<Integer,String> scoremap = new TreeMap<Integer,String>();
	public int calScore(TrustBean tBean) 
	{
		
		if(tBean == null)
		{
			System.out.println("insecure connection");
			return 5;
		}
		else
		{
		int protScore = getProtocolScore(tBean.getProtocol());
		int keyScore = getKeyScore(tBean.getKeySize());
		int caScore = getCAScore(tBean.getCA());
		int algScore = getAlgScore(tBean.getAlgorithm());
		int levelScore = getLevelScore(tBean.getLevel());
		int totalScore = 5+protScore+keyScore+caScore+algScore+levelScore;
		scoremap.put(totalScore,tBean.getSourceName());
		System.out.println(totalScore);
		return totalScore;
		}
	}

	private int getLevelScore(int level) 
	{
		if(level == 2)
		{
			return 5;
		}
		else if(level == 3)
		{
			return 4;
		}
		else if(level == 4)
		{
			return 3;
		}
		else if(level == 5)
		{
			return 2;
		}
		else
		{
			return 1;
		}
	}

	private int getAlgScore(String algorithm) 
	{
		if(algorithm.contains("RSA"))
		{
			return 5;
		}
		else if(algorithm.contains("AES"))
		{
			return 4;
		}
		else if(algorithm.contains("3DES"))
		{
			return 3;
		}
		else if(algorithm.contains("DES"))
		{
			return 2;
		}
		else
		{
			return 1;
		}
	}

	private int getCAScore(String ca) 
	{
		if(ca.contains("GeoTrust")|| ca.contains("Thawte")|| ca.contains("VeriSign"))
		{
			return 5;
		}
		else if(ca.contains("GoDaddy")|| ca.contains("Starfield Technologies")|| ca.contains("DigiCert"))
		{
			return 4;
		}
		else if(ca.contains("COMODO")|| ca.contains("Cybertrust"))
		{
			return 3;
		}
		else if(ca.contains("GlobalSign"))
		{
			return 2;
		}
		else
		{
			return 1;
		}
	}

	private int getKeyScore(String keySize) 
	{
		if(keySize.contains("2048"))
		{
			return 5;
		}
		else if(keySize.contains("1024"))
		{
			return 4;
		}
		else if(keySize.contains("512"))
		{
			return 3;
		}
		else if(keySize.contains("256"))
		{
			return 2;
		}
		else
		{
			return 1;
		}
	}

	private int getProtocolScore(String protocol) 
	{
		if(protocol.contains("TLSv2"))
		{
			return 5;
		}
		else if(protocol.contains("TLSv1"))
		{
			return 4;
		}
		else if(protocol.contains("SSLv3"))
		{
			return 3;
		}
		else if(protocol.contains("SSLv2"))
		{
			return 2;
		}
		else
		{
			return 1;
		}
	}


	
	

}
