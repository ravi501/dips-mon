package com.Logic;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Beans.TrustBean;

/**
 * Servlet implementation class FirstServlet
 */
public class SecondServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public SecondServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 
		/*String dbName = System.getProperty("mydb"); 
		String userName = System.getProperty("awsuser"); 
		String password = System.getProperty("mypassword"); 
		String hostname = System.getProperty("mydbinstance.cqmhmgv63h5u.us-east-1.rds.amazonaws.com");
		String port = System.getProperty("3306");
		
		String jdbcUrl = "jdbc:mysql://" + hostname + ":" + port + "/" + dbName + "?user=" + userName + "&password=" + password;
		*/
		String Host= request.getParameter("host");
		System.out.println(Host);
		GetTrust getTrust = new GetTrust(); 
		TrustBean tBean = getTrust.calculate(Host);
		if(tBean != null)
		{
		request.setAttribute("information", tBean);
		System.out.println(tBean);
		/*RequestDispatcher dispatcher=request.getRequestDispatcher("./results.jsp");
		dispatcher.forward(request, response);*/
		javax.servlet.RequestDispatcher rd = request.getRequestDispatcher("/results1.jsp");
		rd.forward(request, response);
		}
		else
		{
			
			javax.servlet.RequestDispatcher rd = request.getRequestDispatcher("/SSL Checker.jsp");
			rd.forward(request, response);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
