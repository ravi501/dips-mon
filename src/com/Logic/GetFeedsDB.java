package com.Logic;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.Beans.ValueBean;
import com.db.DatabaseConnection;

public class GetFeedsDB 
{

ValueBean valueBean = null;
Connection con= null;
Statement statement = null;
ResultSet rSet= null;
ArrayList<ValueBean> list = null;
 public ArrayList<ValueBean> getFeeds()
{
	list = new ArrayList<ValueBean>();
	con = DatabaseConnection.getConnection();
	try 
	{
	statement=con.createStatement();
	rSet=statement.executeQuery("select * from javatable");
	while(rSet.next())
	{
		valueBean =new ValueBean();
				
		valueBean.setUrl(rSet.getString(1));
		valueBean.setTitle(rSet.getString(2));
		valueBean.setFeed(rSet.getString(3));
		valueBean.setAnchorLink(rSet.getString(4));
		list.add(valueBean);
	
		
		}

	
	return list;
	} 
	catch (SQLException e) 
	{
		e.printStackTrace();
	}
	return list;
}
}
