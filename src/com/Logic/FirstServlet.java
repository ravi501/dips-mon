package com.Logic;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Beans.TrustBean;
import com.Beans.ValueBean;

/**
 * Servlet implementation class FirstServlet
 */
public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public FirstServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		GetFeedsDB feedsClass = new GetFeedsDB();
		RSSReader rssReader = new RSSReader();
		GetScore getScore = new GetScore();
		GetTrust getTrust = new GetTrust();
		rssReader.setFeeds();
		ArrayList<ValueBean> feedsList = new ArrayList<ValueBean>();
		ArrayList<ValueBean> sortList = new ArrayList<ValueBean>();
		TrustBean tBean = null;
		feedsList = feedsClass.getFeeds();
		
		if(feedsList != null)
		{
		for(ValueBean vBean : feedsList)
		{
		tBean = new TrustBean();
		tBean = getTrust.calculate(vBean.getUrl());
		int total = getScore.calScore(tBean);
		vBean.setTrustScore(total);
		sortList.add(vBean);
		if(tBean == null)
		{
			PrintWriter pw = response.getWriter();
			pw.println();
			System.out.println("got the low score");
		}
		else
		{
		System.out.println("beans finished calculating"+tBean.getSourceName());
		}
		}
		Collections.sort(sortList, new Sorting());
		request.setAttribute("sortedfeeds", sortList);
		System.out.println(feedsList);
		javax.servlet.RequestDispatcher rd = request.getRequestDispatcher("/results.jsp");
		rd.forward(request, response);
		
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
