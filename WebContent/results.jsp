<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@page import="com.Beans.TrustBean" %>
<%@page import="com.Beans.ValueBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>DIPS MON-Feeds</title>

<link rel="stylesheet" type="text/css" href="css.css" media="screen" />
</head>

<body>

<div id="container"> 

  <div id="header">
    <div class="headtitle">DIPS-Mon</div>
  </div>

 <div id='cssmenu'>
<ul>
   <li class='active'><a href='index.jsp'>Home</a></li>
   
   
   <li class='last'><a href='SSL Checker.jsp'>SSL Checker</a>
		
   </li>
   <li class='last'><a href='Logic'>Feeds</a>
		
   </li>
   
</ul>
</div>

  <div id="content"> 

    <div id="insidecontent"> <br>
      <h1>Feeds</h1>

	  <div>
	  
	  <table border="0" width = "700px" >
<%
ArrayList<ValueBean> sortedList = (ArrayList<ValueBean>)request.getAttribute("sortedfeeds");
System.out.println(sortedList);
for(ValueBean vBean : sortedList)
{
String feeds = vBean.getFeed();
%>

<tr>
	<td >
	<br /><h3><%=vBean.getTitle() %></h3>
	<p><%=feeds %>&nbsp&nbsp<b>Trust score:<%=vBean.getTrustScore()%></b></p>
	<a href=<%=vBean.getAnchorLink() %>><%=vBean.getAnchorLink() %></a>

	</td>
	
</tr>

<%} %>
</table>
	  
	  
	  
	  
	  
</div>	  
    </div>
	

    <div style="clear: both;"></div>

  </div>



  <div id="footer"> <p>Copyright 2013 | All Rights Reserved | Security Group  </p>
  </div>

</div>
</body>
</html>